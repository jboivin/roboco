<div id="nav-bar">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="btn-container cd-menu-trigger">
                    <div class="open-menu">
                        <div class="hamb">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <span class="txt">menu</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="logo-container">
                    <a href="<?php if($prefix == ""): ?>./<?php else: echo $prefix; endif; ?>" title=""><img src="<?php echo $prefix; ?>img/logo-header.png" alt=""></a>
                </div>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="lang-container">
                    <div class="flex-container">
                        <a href="<?php echo $page_fr; ?>" title="">FR</a>
                        <span></span>
                        <a href="<?php echo $page_en; ?>" title="">EN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cd-shadow-layer"></div>

<nav id="main-nav" class="">
    <ul>
        <li><a href="<?php if($prefix == ""): ?>./<?php else: echo $prefix; endif; ?>"><span>Accueil</span></a></li>
        <li><a href="<?php echo $prefix; ?>renovations"><span>Rénovations générales</span></a></li>
        <li><a href="<?php echo $prefix; ?>agrandissement"><span>Agrandissement de maison</span></a></li>
        <li><a href="<?php echo $prefix; ?>transformation"><span>Transformation de maison</span></a></li>
        <li><a href="<?php echo $prefix; ?>realisations"><span>Réalisations</span></a></li>
        <li><a href="<?php echo $prefix; ?>projets-en-cours/"><span>Projets en cours</span></a></li>
        <li><a href="<?php echo $prefix; ?>contact"><span>Contact</span></a></li>
    </ul>
    <a href="#0" class="cd-close-menu">&times;</a>
</nav>

<div class="menu-effect">