<?php
/* --- PHP FUNCTIONS --- */

/* --- Compare l'url à la valeur donnée. Si vrai, retourne active --- */

function checkCurrentURL($pageName) {
    $currentURL = $_SERVER['PHP_SELF'];
    if (strpos($currentURL, $pageName) !== false) {
        return 'active';
    }
}

/* --- INCLUDES --- */
include '../init.php';
?>

<!DOCTYPE HTML>
<html>

    <?php /* --- HEAD START --- */ ?>
    <head>

        <?php
        /* --- Lien canonique dynamique --- */
        $mystring = $_SERVER['REQUEST_URI'];
        $findme = '.php';
        $pos = strpos($mystring, $findme);
        if ($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index" || $_SERVER['REQUEST_URI'] == "/index.php") {
            ?> <link rel="canonical" href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>" /><?php
        } else {
            if ($pos === false) {
                ?>
                <link rel="canonical" href="<?php echo "http://$_SERVER[HTTP_HOST]" . $_SERVER['REQUEST_URI']; ?>"/>   <?php } else {
                ?>

                <link rel="canonical" href="<?php
                echo "http://$_SERVER[HTTP_HOST]" . str_replace(".php", "", $_SERVER['REQUEST_URI']);
                ;
                ?>"/> 
                      <?php
                  }
              }
              ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="author" href="../humans.txt">

        <?php /* --- Meta description --- */ ?>
        <meta name="description" content="<?php echo $meta_description; ?>">

        <?php /* --- Meta keywords --- */ ?>
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">

        <?php /* --- Titre de la page --- */ ?>
        <title><?php echo ($page_title . ' - ' . $company_name); ?></title>

        <?php /* --- Favicon --- */ ?>
        <link rel="icon" type="image/png" href="../img/favicon.png" />

        <?php /* --- Analytics --- */ ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123516622-26"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
        
            gtag('config', 'UA-123516622-26');
        </script>

    </head>
    <?php /* --- HEAD END --- */ ?>

    <?php /* --- BODY START --- */ ?>
    <body class="<?php echo $body_class; ?> move-out">
        <div id="nav-bar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="btn-container cd-menu-trigger">
                            <div class="open-menu">
                                <div class="hamb">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                menu
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <div class="logo-container">
                            <a href="./" title=""><img src="../img/logo-header.png" alt=""></a>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="lang-container">
                            <div class="flex-container">
                                <a href="<?php echo $page_fr; ?>" title="">FR</a>
                                <span></span>
                                <a href="<?php echo $page_en; ?>" title="">EN</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="cd-shadow-layer"></div>

        <nav id="main-nav" class="">
            <ul>
                <li><a href="./"><span>Home</span></a></li>
                <li><a href="renovations"><span>General renovations</span></a></li>
                <li><a href="extensions"><span>Home extensions</span></a></li>
                <li><a href="transformation"><span>Transforming your home</span></a></li>
                <li><a href="our-work"><span>Our work</span></a></li>
                <li><a href="contact"><span>Contact</span></a></li>
            </ul>
            <a href="#0" class="cd-close-menu">&times;</a>
        </nav>

        <div class="menu-effect">