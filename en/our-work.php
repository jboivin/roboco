<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Our work";
$body_class = "real-content";
$page_fr = "../realisations";
$page_en = "our-work";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>Some<br>Projects</h1>

                            <p>Robco Renovation Construction continues to expand its works in the residential sector and especially in the transformation of plex into cottages.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RÉALISATIONS --- */ ?>
    <section id="realisations" class="">
        <div class="container-fluid">
            <div class="single-cat">
                <h2 class="bordered">general renovations</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 45; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="../img/renovations/full/renovations-generales-' . $nb_courant . '.jpg" class="single-img" data-fancybox="reno-projet" title="">';
                        $html .= '<img src="../img/renovations/thumbs/renovations-generales-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>click<br>to<br>enlarge</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>

            <div class="single-cat">
                <h2 class="bordered">Home extensions</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 16; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="../img/agrandissement/full/agrandissement-maison-' . $nb_courant . '.jpg" class="single-img" data-fancybox="agr-projet" title="">';
                        $html .= '<img src="../img/agrandissement/thumbs/agrandissement-maison-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>click<br>to<br>enlarge</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>

            <div class="single-cat">
                <h2 class="bordered">Transforming your home</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 6; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="../img/transformation/full/transformation-maison-' . $nb_courant . '.jpg" class="single-img" data-fancybox="trans-projet" title="">';
                        $html .= '<img src="../img/transformation/thumbs/transformation-maison-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>click<br>to<br>enlarge</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RÉALISATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>