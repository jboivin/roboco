<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "General renovations";
$body_class = "reno-content";
$page_fr = "../renovations";
$page_en = "renovations";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>GENERAL<br>RENOVATIONS</h1>

                            <p><strong>Renovating your kitchen, bathroom and basement can add value to your home.</strong></p>

                            <p>Be it a renovation to increase value or simply a makeover, Robco Renovation Construction will keep in mind not only your desires, but also your budget.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Renovating the kitchen, the heart of the home:</h2>

                            <p>Apart from being the ultimate gathering place, the kitchen is also the hub of daily activities in your home. It is therefore vital to make it ergonomic and optimize storage. Robco Renovation Construction will first study your living habits to suggest solutions adapted to your taste and special requests.</p>

                            <p>We will seize the moment to review your kitchen’s design. It’ll be functional, yet reflect your taste while being modern and trendy. A wide array of cabinets, counters and accessories are available to you on the market, and we will guide you through the process. Renovating your kitchen is the perfect opportunity to reorganize your space and change your appliances. Two birds one stone.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/renovations/renovation-cuisine.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno reverse">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="img-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/renovations/renovation-salledebain.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="text-container">
                            <h2 class="bordered">Renovating an obsolete bathroom:</h2>

                            <p>Your bathroom is a disaster? You’re looking to make it more comfortable? You would like to recreate an oasis, a harbour or a seashore on a smaller scale? It’s full of feeble and outdated elements? You would like to extend it? Our business is there for your every need.</p>

                            <p>Be it putting in a bathtub, a shower, a tub/shower, finding the appropriate plumbing and vanity, installing a linen room, a heated floor or a high quality faucet, Robco Renovation Construction will work hard to find the appropriate balance between your existing bathroom and novelty. </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">A finished basement, an added bonus:</h2>

                            <p>Are you already using all the habitable space of your home, but have not yet taken advantage of the available space in the basement? Now is the time to seriously consider it. Robco Renovation Construction will keep in mind the structural constraints of the basement to design a new custom-made room.</p>

                            <p>Even if excavation work might be necessary, we will of course keep you informed of the process from beginning to end. The business will also give you judicious advice on which materials are necessary when it comes to finishing and renovating your basement.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/renovations/renovation-soussol.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>