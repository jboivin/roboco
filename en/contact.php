<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Contact";
$body_class = "contact-content";
$page_fr = "../contact";
$page_en = "contact";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>CONTACT</h1>

                            <p><strong>For more information on our services, fill out the enclosed form.</strong></p>

                            <p>We will gladly contact you as soon as possible.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>
    
    <?php /* --- CONTACT --- */ ?>
    <section id="contact" class="">
        <div class="box" data-aos="fade-right" data-aos-anchor-placement="center-bottom"></div>
        
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <h2 class="bordered">general<br>informations</h2>
                        
                        <p>2702, Knox, Montreal<br>Quebec, H3K 1R4</p>
                        
                        <div class="coord">
                            <img src="../img/contact/icon-telephone.png">
                            
                            <div class="text-block">
                                <p><strong>Phone</strong></p>
                                <p><a href="tel:15147658747">514-765-8747</a></p>
                            </div>
                        </div>
                        
                        <div class="coord">
                            <img src="../img/contact/icon-fax.png">
                            
                            <div class="text-block">
                                <p><strong>Fax</strong></p>
                                <p>514-765-0981</p>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="form-container">
                        <?php include 'inc/form.php'; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- CONTACT END --- */ ?>
    
    <div class="map otherlang"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d11190.959047269533!2d-73.566036!3d45.475046!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc44e2c145e49e7aa!2sRobco+Renovation+Construction+Inc!5e0!3m2!1sfr!2sca!4v1536260787574" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>
    
</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>