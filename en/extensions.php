<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Home extensions";
$body_class = "agr-content";
$page_fr = "../agrandissement";
$page_en = "extensions";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>HOME<br>EXTENSIONS</h1>

                            <p><strong>The actual state of your home is no longer convenient, you need more space but don’t want to move because you like the location?</strong></p>

                            <p>You would rather take advantage of the residence you already have, invest in it and increase its property value?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Possibilities</h2>

                            <ul>
                                <li data-aos="fade-up"><strong>Adding a floor</strong><br>If the bearing capacity of the roof is solid enough to support a second floor, add one over your garage.</li>
                                <li data-aos="fade-up"><strong>Cantilever extension</strong><br>Extend a room by four feet, a little like if you added a terrace, without needing excavation.</li>
                                <li data-aos="fade-up"><strong>Extending the back</strong><br>Encroach on your backyard with an extension that spreads along the width of your home.</li>
                                <li data-aos="fade-up"><strong>Digging the basement</strong><br>Enjoy additional space by digging a basement. Make the most of it and at the same time strengthen your foundations.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/agrandissement/agrandissement-possibilités.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno reverse">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="img-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/agrandissement/agrandissement-avantages.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="text-container small">
                            <h2 class="bordered">Advantages</h2>

                            <p>Expanding your home also means not having to buy a bigger house who would cost more in heating and electricity.</p>

                            <p>In fact, during the construction work, we take the opportunity to insulate the building and replace old materials by new ones that are greener and less damageable for your health.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Warning!!!</h2>

                            <p>In most cases, extending the front of your home is prohibited. An extension that doesn’t match the rest of your home can ruin the landscape and devalue it. Another reason why you need the expert advice of a qualified contractor, just like Robco Renovation Construction can give to you.</p>

                            <p>If your plot of land and town planning rules allow it, you could extend the height, the front, the back, the sides, or simply rearrange an unused space. All you need to do is add a floor, make use of the attic, your garage or basement. And why not take the opportunity to add a superb mezzanine? At the same time, your home would benefit from more natural light.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/agrandissement/agrandissement-attention.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>
    
</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>