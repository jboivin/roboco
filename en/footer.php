<?php /* --- BANDE REALISATIONS --- */ ?>
<?php if (!checkCurrentURL("our-work")): ?>
    <section id="bande-realisations">
        <div class="container-fluid">
            <div class="box" data-aos="fade-right" data-aos-anchor-placement="center-bottom"></div>

            <div class="text-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
                <h2 class="bordered">OUR<br>WORK</h2>

                <p>Robco Renovation Construction is continuously multiplying its achievements in the residential sector, particularly in the transformation of plexes into townhouses.</p>

                <a class="btn gray-btn" href="our-work" title="">learn more</a>
            </div>

            <img src="../img/footer/photo-realisations.jpg" alt="">
        </div>
    </section>
<?php endif; ?>
<?php /* --- BANDE REALISATIONS END --- */ ?>


<?php /* --- FOOTER START --- */ ?>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 col-sm-o-2">
                <div class="coord-container">
                    <p>Phone : <a href="tel:15147658747"><strong>514-765-8747</strong></a></p>
                    <p>Fax : <a href="tel:15147650981"><strong>514-765-0981</strong></a></p>
                    <p>2702, Knox, Montréal, Qc, H3K 1R4</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12 col-sm-o-1">
                <div class="logo-container">
                    <a href="./" title=""><img src="../img/logo-footer.png" alt=""></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 col-sm-o-3">
                <div class="text-container">
                    <p>Robco Renovation Construction operates in the residential sector on the Island of Montreal and its surroundings. Contact our business now to renovate your kitchen, your bathroom, to finish your basement or to extend your home.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php /* --- FOOTER END --- */ ?>

<section id="ino-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="text-right">Robco <?php echo date("Y"); ?> - All rights reserved</p>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Powered by : <a href="https://inovision.ca/" title="Inovision création de site internet Montréal & Rive-sud - Canada" target="_blank">inovision.ca</a></p>
            </div> 
        </div>
    </div>
</section>
</div>

<?php /* --- STYLES CSS --- */ ?>
<link href="../css/styles.css" rel="stylesheet" type="text/css">

<?php /* --- SCRIPTS JS --- */ ?>
<script defer src="../js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script defer src="../js/bootstrap.min.js" type="text/javascript"></script>
<script src="../js/aos.js" type="text/javascript"></script>
<script defer src="../js/paroller.js" type="text/javascript"></script>
<?php if (checkCurrentURL("contact")): ?>
    <script defer src="https://www.google.com/recaptcha/api.js?hl=en" type="text/javascript"></script>
<?php endif; ?>
<?php if (checkCurrentURL("our-work")): ?>
    <script defer src="../js/fancybox.js" type="text/javascript"></script>
<?php endif; ?>
<script defer src="../js/scripts.js" type="text/javascript"></script>

</body>
</html>