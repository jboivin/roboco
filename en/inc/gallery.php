<?php
$html = "";

for ($index = 1; $index <= 10; $index++) {
    $nb_courant;
    if ($index < 10) {
        $nb_courant = '0' . $index;
    } else {
        $nb_courant = $index;
    }
    $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
    $html .= '<a href="img/' . $nb_courant . '.jpg" class="single-img" data-fancybox="' . $company_name . '" title="">';
    $html .= '<img src="img/' . $nb_courant . '.jpg" alt="">';
    $html .= '<p>cliquez pour agrandir</p>';
    $html .= '</a>';
    $html .= '</div>';
}

echo $html;
?>
