<form action="../mail.php" method="post">
    <input type="hidden" name="lang" value="en">
    
    <?php if (isset($_GET["success"])): ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Success!</strong> Your request has been sent.
        </div>
    <?php endif; ?>

    <?php if (isset($_GET["recaptcha"])): ?>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning!</strong> Be sure to check the reCAPTCHA validation before sending.
        </div>
    <?php endif; ?>

    <?php if (isset($_GET["missing"])): ?>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Warning!</strong> Make sure you fill out all the fields before sending.
        </div>
    <?php endif; ?>

    <input type="text" name="fullname" placeholder="Full name" required>

    <input type="email" name="email" placeholder="Email" required>

    <input type="tel" name="phone" placeholder="Phone" required>

    <textarea name="message" placeholder="Message" required></textarea>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div style="overflow: hidden;">
                <div class="g-recaptcha" data-sitekey="<?php echo $website_captcha_key; ?>"></div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="text-right">
                <input type="submit" class="btn" value="SUBMIT">
            </div>
        </div>
    </div>
</form>