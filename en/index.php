<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Home";
$body_class = "";
$page_fr = "../";
$page_en = "./";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>

    <?php /* --- HERO ACCUEIL --- */ ?>
    <section id="hero-accueil" class="">
        <div class="container-fluid">
            <h1>renovation <span class="yellow">&amp;</span> Construction</h1>
            <h2>in the greater montreal</h2>
        </div>
    </section>
    <?php /* --- HERO ACCUEIL END --- */ ?>

    <?php /* --- BANDE ESTIMATION --- */ ?>
    <section id="bande-estimation" class="">
        <div class="container-fluid">
            <h3>Contact us for an estimate</h3>

            <a class="btn" href="tel:15147658747" title="">514-765-8747</a>
        </div>
    </section>
    <?php /* --- BANDE ESTIMATION END --- */ ?>

    <?php /* --- ABOUT HOME --- */ ?>
    <section id="about-home" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <h2 class="bordered">About us</h2>

                        <p class="subtitle" data-aos="fade-up"><strong>Founded in the year 2000 by Robert Boudreau, a trained carpenter-joiner with over 33 years of experience, Robco Renovation Construction is a certified Réno-maître by APCHQ and Novoclimat.</strong></p>

                        <p data-aos="fade-up">Our business operates in the residential sector on the Island of Montreal. Thanks to the small structure of our business, the customer pays a fair price and services are rendered in time. Whatever happens, Robert Boudreau, owner, is always on site. For a quality experience, you can count on Robco Renovation Construction! </p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="img-container">
                        <img src="../img/accueil/accueil-apropos-photo.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- ABOUT HOME END --- */ ?>

    <?php /* --- INTRO SERVICES --- */ ?>
    <section id="intro-services" class="" data-paroller-factor="0.4">
        <div class="container-fluid">
            <h2>Our business carries out renovations in the residential sector on the Island of Montreal and its surroundings. </h2>

            <h3>Robco Renovation Construction has you covered for all your kitchen, bathroom, basement or home extension needs.</h3>
        </div>
    </section>
    <?php /* --- INTRO SERVICES END --- */ ?>

    <?php /* --- SERVICES --- */ ?>
    <section id="services-home" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="single-serv" data-aos="fade-up">
                        <a href="renovations" title="" class="img-container">
                            <img src="../img/accueil/services/serv-1.jpg" alt="">
                        </a>

                        <h4><a href="renovations" title="">General<br>renovations</a></h4>

                        <p>Need more space in your kitchen? Your bathroom could use a little makeover and you want to make it more comfortable? We are the solution to all your renovation needs. </p>

                        <a class="cust-btn" href="renovations" title="">learn more</a>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="flex-container text-center">
                        <div class="single-serv" data-aos="fade-up" data-aos-delay="200">
                            <a href="extensions" title="" class="img-container">
                                <img src="../img/accueil/services/serv-2.jpg" alt="">
                            </a>

                            <h4><a href="extensions" title="">Home<br>extensions</a></h4>

                            <p>Do you need more natural light and would like to rethink the inside-outside relationship of your home? To help you find solutions on how to open up your space and on extension projects, our business sees big!</p>

                            <a class="cust-btn" href="extensions" title="">learn more</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="flex-container text-right">
                        <div class="single-serv" data-aos="fade-up" data-aos-delay="400">
                            <a href="transformation" title="" class="img-container">
                                <img src="../img/accueil/services/serv-3.jpg" alt="">
                            </a>

                            <h4 class="one-line"><a href="transformation" title="">Transformation</a></h4>

                            <p>Are you already maximizing space from the ground floor to the attic? For excavation work, nobody beats Robco Renovation Construction.</p>

                            <a class="cust-btn" href="transformation" title="">learn more</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SERVICES END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>