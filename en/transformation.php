<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Transforming your home";
$body_class = "trans-content";
$page_fr = "../transformation";
$page_en = "transformation";

/* --- INCLUDE HEADER --- */
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>

    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>TRANSFORMING<br>YOUR HOME</h1>

                            <p><strong>In an urban environment, possibilities are limited for families. Owning a spacious home on the Island of Montreal is rarer than we would think.</strong></p>

                            <p>So, why not consider the clever and popular idea to transform your duplex or triplex into a townhouse? Wouldn’t that be the result of our growing multigenerational reality?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- TRANSFORMATION --- */ ?>
    <section id="transformation">
        <div class="container-fluid">
            <p class="title">Conversion renovation work is very popular for good reason. We adapt our apartments to modern life, while keeping your savings in mind. </p>

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-patrimoine.png" alt="">
                        </div>

                        <h3>Heritage<br>conservation</h3>

                        <p>In order to get the most out of your plex and optimize existing space, Robco Renovation Construction will conserve its original frame. Your home will keep its authenticity, character, and touch of charm. Nevertheless, Robco Renovation Construction isn’t intimidated by anything: 1900’s homes are our business. </p>
                    </div>  
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-decloisonner.png" alt="">
                        </div>

                        <h3>Decompartmentalize<br>space</h3>

                        <p>To decompartmentalize space, create openings and get rid of the feeling of smallness, modifications will be made to the structure of the building. Conversion renovation work requires demolition. Tearing down bearing walls means replacing them by beams, columns and purlins to compensate for their removal.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-air-ouverte.png" alt="">
                        </div>

                        <h3>Open<br>spaces</h3>

                        <p>Nothing beats creating open spaces that connect on the ground floor like the entrance, the kitchen, the living room and the office for example, to give the impression that the space is roomier, vaster even. As it happens, we carry out this type of work.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-poutres.png" alt="">
                        </div>

                        <h3>Bearing walls<br>and beams</h3>

                        <p>We offer a bearing wall removal service to open up space and make it less cramped to facilitate movement. Bearing beams are more easily hidden and can blend in with the décor, avoiding compartmentalization and obstruction of space. By doing so, you can allow the light to flow in.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-escalier.png" alt="">
                        </div>

                        <h3>Building a staircase<br>that connects two floors</h3>

                        <p>We also offer basement excavation, and with a basement comes a staircase. Our carpenter-joiners will design a beautiful custom made staircase to connect the two floors. If necessary, we can even create a light shaft for the top floors to illuminate your stairwell.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon-container">
                            <img src="../img/transformation/icon-salles-de-bain.png" alt="">
                        </div>

                        <h3>Moving or designing<br>a bathroom</h3>

                        <p>Be it moving or designing a new bathroom, we have the expertise and the equipment to rearrange your piping.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- TRANSFORMATION END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Finishing<br>the basement</h2>

                            <p>Investing in your basement to store your things, install a bar, a storeroom, a cinema, a workshop, a bedroom with an adjacent bathroom, a family room, a playroom or an exercise room is a wonderful idea to improve the quality of life of all occupants.</p>

                            <p>Robco Renovation Construction knows perfectly that the most successful extensions work with what is already there, while enhancing the charm of the exterior. For an extension on foundations, excavation work or heightening of your roof, Robco Renovation Construction is what you need. </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="../img/transformation/transformation-soussol.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
?>