<?php /* --- FOOTER START --- */ ?>
<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12 col-sm-o-2">
                <div class="coord-container">
                    <p>Téléphone : <a href="tel:15147658747"><strong>514-765-8747</strong></a></p>
                    <p>Télécopieur : <a href="tel:15147650981"><strong>514-765-0981</strong></a></p>
                    <p>2702, Knox, Montréal, Qc, H3K 1R4</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-12 col-xs-12 col-sm-o-1">
                <div class="logo-container">
                    <a href="<?php if($prefix == ""): ?>./<?php else: echo $prefix; endif; ?>" title=""><img src="<?php echo $prefix; ?>img/logo-footer.png" alt=""></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 col-sm-o-3">
                <div class="text-container">
                    <p>Robco Rénovation Construction œuvre sur l’Ile de Montréal et les environs dans le secteur résidentiel. Sans plus tarder, contactez l’entreprise pour rénover votre cuisine, votre salle de bain, finir votre sous-sol ou effectuer un agrandissement.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php /* --- FOOTER END --- */ ?>

<section id="ino-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <p class="text-right">Robco <?php echo date("Y"); ?> - Tous droits réservé</p>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <p>Conception : <a href="https://inovision.ca/" title="Inovision création de site internet Montréal & Rive-sud - Canada" target="_blank">inovision.ca</a></p>
            </div> 
        </div>
    </div>
</section>
</div>