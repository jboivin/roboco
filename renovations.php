<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Rénovations générales";
$body_class = "reno-content";
$page_fr = "renovations";
$page_en = "en/renovations";

/* --- INCLUDE HEADER --- */
include 'head.php';
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>RénovationS<br>GÉNÉRALES</h1>

                            <p><strong>Rénover sa cuisine, sa salle de bain et son sous-sol accroît la valeur marchande de la maison.</strong></p>

                            <p>Que ce soit pour une rénovation économique ou une remise à neuf, Robco Rénovation Construction tiendra compte non seulement de vos désirs, mais de votre budget.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Rénover sa cuisine, le cœur de la maison</h2>

                            <p>En plus d’être le lieu de rassemblement par excellence, la cuisine, c’est de loin le centre névralgique des opérations quotidiennes le plus achalandé à l’intérieur de votre foyer. Il est donc impératif d’adapter son ergonomie et d’optimiser son rangement. Robco Rénovation Construction étudiera d’abord vos habitudes de vie pour vous proposer des solutions qui tiendront compte de vos goûts et demandes particulières.</p>

                            <p> L’entreprise profitera du moment pour revoir le design de votre cuisine. Fonctionnelle, elle sera à votre image, actualisée et tendance. Un grand choix d’armoires, de comptoirs et d’accessoires s’offre à vous sur le marché et l’entreprise saura vous guider. Rénover sa cuisine, c’est par le fait même, l’occasion rêvée pour réorganiser ou changer ses électroménagers. Faites d’une pierre deux coups.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/renovations/renovation-cuisine.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno reverse">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="img-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/renovations/renovation-salledebain.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="text-container">
                            <h2 class="bordered">Rénover une salle de bain obsolète</h2>

                            <p>Votre salle de bain ressemble à une catastrophe? Vous recherchez davantage de confort? Vous aimeriez recréer l’ambiance d’une oasis, d’un havre ou d’un bord de mer à petite échelle? Votre ancienne salle d’eau est remplie d’éléments déficients et désuets? Vous aimeriez l’agrandir? Notre entreprise est là pour voir à vos besoins.</p>

                            <p>S’il s’agit de la munir d’une baignoire, d’une douche ou d’un bain/douche assortis, de trouver les sanitaires appropriés ainsi que la vanité qu’il lui faut, d’aménager une lingerie, d’installer un plancher chauffant ou une robinetterie haute gamme, Robco Rénovation Construction solutionnera les problèmes pour résoudre l’adéquation parfaite entre l’existant et la nouveauté.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">La finition du<br>sous-sol,<br>un gros plus</h2>

                            <p>Vous occupez toute la surface habitable de votre maison, mais vous n’avez pas encore exploité l’espace disponible au sous-sol? C’est le temps ou jamais d’y songer sérieusement. Robco Rénovation Construction considérera les contraintes structurelles de votre sous-sol afin de concevoir une pièce nouvelle sur mesure.</p>

                            <p>Même si des travaux d’excavation seront peut-être nécessaires, cela va sans dire, nous vous tiendrons informé du processus du début à la fin des travaux. L’entreprise vous prodiguera aussi de judicieux conseils pour que vous effectuiez les bons choix de matériaux nécessaires quant à la rénovation et la finition de votre sous-sol.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/renovations/renovation-soussol.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'inc/realisations.php';
include 'footer.php';
include 'scripts.php';
?>