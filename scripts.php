<?php /* --- STYLES CSS --- */ ?>
<link href="css/styles.css" rel="stylesheet" type="text/css">

<?php /* --- SCRIPTS JS --- */ ?>
<script defer src="js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script defer src="js/bootstrap.min.js" type="text/javascript"></script>
<script src="js/aos.js" type="text/javascript"></script>
<script defer src="js/paroller.js" type="text/javascript"></script>
<?php if (checkCurrentURL("contact")): ?>
    <script defer src="https://www.google.com/recaptcha/api.js?hl=fr" type="text/javascript"></script>
<?php endif; ?>
<?php if (checkCurrentURL("realisations")): ?>
    <script defer src="js/fancybox.js" type="text/javascript"></script>
<?php endif; ?>
<script defer src="js/scripts.js" type="text/javascript"></script>

</body>
</html>