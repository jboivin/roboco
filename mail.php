<?php

include 'init.php';

if (isset($_POST["fullname"]) && isset($_POST["email"]) && isset($_POST["phone"]) && isset($_POST["message"])) {

    if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {

        $secret = $secret_captcha_key;

        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
    }

    $responseData = json_decode($verifyResponse);

    if ($responseData->success) {
        $fullname = $_POST["fullname"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $message = $_POST["message"];

        $to = $contact_mail;
        $subject = "Contact - Site Web";

        $emailmessage = 'Nom Complet : ' . $fullname . "\r\n" .
                'Courriel : ' . $email . "\r\n" .
                'Téléphone : ' . $phone . "\r\n" . "\r\n" .
                $message . "\r\n" . "\r\n" .
                $company_name . "\r\n";

        $headers = 'From: ' . $fullname . ' <' . $email . '> ' . "\r\n" .
                'Reply-To: ' . $email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $emailmessage, $headers);

        if (isset($_POST["lang"])) {
            header('Location: en/contact?success');
            exit;
        } else {
            header('Location: contact?success');
            exit;
        }
    } else if (isset($_POST["lang"])) {
        header('Location: en/contact?recaptcha');
        exit;
    } else {
        header('Location: contact?recaptcha');
        exit;
    }
} else if (isset($_POST["lang"])) {
    header('Location: en/contact?missing');
    exit;
} else {
    header('Location: contact?missing');
    exit;
}
?>