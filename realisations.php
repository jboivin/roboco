<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Réalisations";
$body_class = "real-content";
$page_fr = "realisations";
$page_en = "en/our-work";

/* --- INCLUDE HEADER --- */
include 'head.php';
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>quelques<br>réalisations</h1>

                            <p>Robco Rénovation Construction ne cesse de multiplier ses réalisations dans le secteur résidentiel et spécialement dans la transformation des plex en cottages.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RÉALISATIONS --- */ ?>
    <section id="realisations" class="">
        <div class="container-fluid">
            <div class="single-cat">
                <h2 class="bordered">rénovations générales</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 45; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="img/renovations/full/renovations-generales-' . $nb_courant . '.jpg" class="single-img" data-fancybox="reno-projet" title="">';
                        $html .= '<img src="img/renovations/thumbs/renovations-generales-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>cliquez<br>pour<br>agrandir</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>

            <div class="single-cat">
                <h2 class="bordered">agrandissement de maison</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 16; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="img/agrandissement/full/agrandissement-maison-' . $nb_courant . '.jpg" class="single-img" data-fancybox="agr-projet" title="">';
                        $html .= '<img src="img/agrandissement/thumbs/agrandissement-maison-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>cliquez<br>pour<br>agrandir</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>

            <div class="single-cat">
                <h2 class="bordered">transformation de maison</h2>

                <div class="row">
                    <?php
                    $html = "";

                    for ($index = 1; $index <= 6; $index++) {
                        $nb_courant;
                        if ($index < 10) {
                            $nb_courant = '0' . $index;
                        } else {
                            $nb_courant = $index;
                        }
                        $html .= '<div class="col-md-3 col-sm-6 col-xs-12">';
                        $html .= '<a href="img/transformation/full/transformation-maison-' . $nb_courant . '.jpg" class="single-img" data-fancybox="trans-projet" title="">';
                        $html .= '<img src="img/transformation/thumbs/transformation-maison-' . $nb_courant . '.jpg" alt="">';
                        $html .= '<p>cliquez<br>pour<br>agrandir</p>';
                        $html .= '</a>';
                        $html .= '</div>';
                    }

                    echo $html;
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RÉALISATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'footer.php';
include 'scripts.php';
?>