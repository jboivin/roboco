<?php
/* --- PHP FUNCTIONS --- */

/* --- Compare l'url à la valeur donnée. Si vrai, retourne active --- */

function checkCurrentURL($pageName) {
    $currentURL = $_SERVER['PHP_SELF'];
    if (strpos($currentURL, $pageName) !== false) {
        return 'active';
    }
}

/* --- INCLUDES --- */
include 'init.php';
$prefix = "";
?>

<!DOCTYPE HTML>
<html>

    <?php /* --- HEAD START --- */ ?>
    <head>

        <?php
        /* --- Lien canonique dynamique --- */
        $mystring = $_SERVER['REQUEST_URI'];
        $findme = '.php';
        $pos = strpos($mystring, $findme);
        if ($_SERVER['REQUEST_URI'] == "/" || $_SERVER['REQUEST_URI'] == "/index" || $_SERVER['REQUEST_URI'] == "/index.php") {
            ?> <link rel="canonical" href="<?php echo "http://$_SERVER[HTTP_HOST]"; ?>" /><?php
        } else {
            if ($pos === false) {
                ?>
                <link rel="canonical" href="<?php echo "http://$_SERVER[HTTP_HOST]" . $_SERVER['REQUEST_URI']; ?>"/>   <?php } else {
                ?>

                <link rel="canonical" href="<?php
                echo "http://$_SERVER[HTTP_HOST]" . str_replace(".php", "", $_SERVER['REQUEST_URI']);
                ;
                ?>"/> 
                      <?php
                  }
              }
              ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="google-site-verification" content="rPRq77WIZq81G_qpCosdbx0epUW_WlWFkaW5PJYUYCI" />
        <link rel="author" href="humans.txt">

        <?php /* --- Meta description --- */ ?>
        <meta name="description" content="<?php echo $meta_description; ?>">

        <?php /* --- Meta keywords --- */ ?>
        <meta name="keywords" content="<?php echo $meta_keywords; ?>">

        <?php /* --- Titre de la page --- */ ?>
        <title><?php echo ($page_title . ' - ' . $company_name); ?></title>

        <?php /* --- Favicon --- */ ?>
        <link rel="icon" type="image/png" href="img/favicon.png" />

        <?php /* --- Analytics --- */ ?>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-123516622-26"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-123516622-26');
        </script>

    </head>
    <?php /* --- HEAD END --- */ ?>

    <?php /* --- BODY START --- */ ?>
    <body class="<?php echo $body_class; ?> move-out">