<form action="mail.php" method="post">
    <?php if (isset($_GET["success"])): ?>
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Succès!</strong> Votre demande de contact a bien été envoyée.
        </div>
    <?php endif; ?>

    <?php if (isset($_GET["recaptcha"])): ?>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Attention!</strong> Assurez-vous de cocher la validation reCAPTCHA avant d'envoyer.
        </div>
    <?php endif; ?>

    <?php if (isset($_GET["missing"])): ?>
        <div class="alert alert-warning alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Attention!</strong> Assurez-vous de remplir tous les champs avant d'envoyer.
        </div>
    <?php endif; ?>

    <input type="text" name="fullname" placeholder="Nom complet" required>

    <input type="email" name="email" placeholder="Courriel" required>

    <input type="tel" name="phone" placeholder="Téléphone" required>

    <textarea name="message" placeholder="Message" required></textarea>

    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div style="overflow: hidden;">
                <div class="g-recaptcha" data-sitekey="<?php echo $website_captcha_key; ?>"></div>
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="text-right">
                <input type="submit" class="btn" value="ENVOYER">
            </div>
        </div>
    </div>
</form>