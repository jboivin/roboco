<?php /* --- BANDE REALISATIONS --- */ ?>
<section id="bande-realisations">
    <div class="container-fluid">
        <div class="box" data-aos="fade-right" data-aos-anchor-placement="center-bottom"></div>

        <div class="text-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
            <h2 class="bordered">nos<br>réalisations</h2>

            <p>Robco Rénovation Construction ne cesse de multiplier ses réalisations dans le secteur résidentiel et spécialement dans la transformation des plex en cottages.</p>

            <a class="btn gray-btn" href="realisations" title="">en savoir plus</a>
        </div>

        <img src="img/footer/photo-realisations.jpg" alt="">
    </div>
</section>
<?php /* --- BANDE REALISATIONS END --- */ ?>