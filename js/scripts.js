jQuery(function ($) {
    $(".btn-menu").click(function () {
        $("body").toggleClass("menu-open");
    });

    AOS.init({
        disable: 'mobile'
    });

    if ($(window).width() > 992) {
        $("[data-paroller-factor]").paroller();
    }

    if ($(window).width() > 767) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $("#nav-bar").addClass("shrink");
            } else {
                $("#nav-bar").removeClass("shrink");
            }
        });
    }
});

jQuery(document).ready(function ($) {
    //open menu
    $('.cd-menu-trigger').on('click', function (event) {
        event.preventDefault();
        $('.menu-effect').addClass('move-out');
        $('#main-nav').addClass('is-visible');
        $('.cd-shadow-layer').addClass('is-visible');
    });
    //close menu
    $('.cd-close-menu').on('click', function (event) {
        event.preventDefault();
        $('.menu-effect').removeClass('move-out');
        $('#main-nav').removeClass('is-visible');
        $('.cd-shadow-layer').removeClass('is-visible');
    });
});

//Smooth scroll
jQuery(function ($) {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});