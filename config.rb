http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"
fonts_dir = "fonts"

relative_assets = true
line_comments = true

output_style = :compressed
# or :nested or :compact or :compressed