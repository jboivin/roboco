<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Accueil";
$body_class = "";
$page_fr = "./";
$page_en = "en/";

/* --- INCLUDE HEADER --- */
include 'head.php';
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>

    <?php /* --- HERO ACCUEIL --- */ ?>
    <section id="hero-accueil" class="">
        <div class="container-fluid">
            <h1>Rénovation <span class="yellow">&amp;</span> Construction</h1>
            <h2>résidentiel à montréal</h2>
        </div>
    </section>
    <?php /* --- HERO ACCUEIL END --- */ ?>

    <?php /* --- BANDE ESTIMATION --- */ ?>
    <section id="bande-estimation" class="">
        <div class="container-fluid">
            <h3>Contactez-nous pour une estimation</h3>

            <a class="btn" href="tel:15147658747" title="">514-765-8747</a>
        </div>
    </section>
    <?php /* --- BANDE ESTIMATION END --- */ ?>

    <?php /* --- ABOUT HOME --- */ ?>
    <section id="about-home" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <h2 class="bordered">à propos<br>de nous</h2>

                        <p class="subtitle" data-aos="fade-up"><strong>Fondée en 2000 par Robert Bourdeau, un charpentier menuisier de formation comptant plus de 33 ans d’expérience, Robco Rénovation Construction est détentrice d’accréditations telles que Réno-maître de l'APCHQ et Novoclimat. </strong></p>

                        <p data-aos="fade-up">Notre entreprise œuvre sur l’Ile de Montréal et les environs dans le secteur résidentiel. Grâce à la petite structure de l’entreprise, le client paie le  prix juste et les travaux sont livrés dans les temps requis. Quoi qu’il arrive, Robert Boudreau, propriétaire, est toujours sur place. Pour que la qualité soit au rendez-vous, comptez sur Robco Rénovation Construction !</p>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="img-container">
                        <img src="img/accueil/accueil-apropos-photo.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- ABOUT HOME END --- */ ?>

    <?php /* --- INTRO SERVICES --- */ ?>
    <section id="intro-services" class="" data-paroller-factor="0.4">
        <div class="container-fluid">
            <h2>Notre entreprise effectue de la rénovation dans le secteur résidentiel sur l’Ile de Montréal et les environs. </h2>

            <h3>Pour la cuisine, la salle de bain, le sous-sol ou un agrandissement, Robco Rénovation Construction se charge de vos travaux.</h3>
        </div>
    </section>
    <?php /* --- INTRO SERVICES END --- */ ?>

    <?php /* --- SERVICES --- */ ?>
    <section id="services-home" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="single-serv" data-aos="fade-up">
                        <a href="renovations" title="" class="img-container">
                            <img src="img/accueil/services/serv-1.jpg" alt="">
                        </a>

                        <h4><a href="renovations" title="">Rénovations<br>générales</a></h4>

                        <p>Votre cuisine cloisonnée vous embête? Votre salle de bain mériterait d’être rajeunie et vous voudriez améliorer son confort? Nous sommes l’option derrière toutes ces questions.</p>

                        <a class="cust-btn" href="renovations" title="">en savoir plus</a>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="flex-container text-center">
                        <div class="single-serv" data-aos="fade-up" data-aos-delay="200">
                            <a href="agrandissement" title="" class="img-container">
                                <img src="img/accueil/services/serv-2.jpg" alt="">
                            </a>

                            <h4><a href="agrandissement" title="">Agrandissement<br>de maison</a></h4>

                            <p>Vous manquez d’espace et de luminosité et vous aimeriez repenser la relation dedans-dehors? Pour trouver des solutions d’ouverture et des projets d’agrandissement, notre entreprise voit grand!</p>

                            <a class="cust-btn" href="agrandissement" title="">en savoir plus</a>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="flex-container text-right">
                        <div class="single-serv" data-aos="fade-up" data-aos-delay="400">
                            <a href="transformation" title="" class="img-container">
                                <img src="img/accueil/services/serv-3.jpg" alt="">
                            </a>

                            <h4 class="one-line"><a href="transformation" title="">Transformation</a></h4>

                            <p>Vous exploitez déjà l’espace maximum de votre maisonnée du rez-de-chaussée au grenier? Pour des travaux d’excavation, rien de mieux que Robco Rénovation Construction.</p>

                            <a class="cust-btn" href="transformation" title="">en savoir plus</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SERVICES END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'inc/realisations.php';
include 'footer.php';
include 'scripts.php';
?>