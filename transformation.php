<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Transformation de maison";
$body_class = "trans-content";
$page_fr = "transformation";
$page_en = "en/transformation";

/* --- INCLUDE HEADER --- */
include 'head.php';
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>

    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>transformation<br>de maisons</h1>

                            <p><strong>En milieu urbain, les possibilités sont limitées pour les familles. Être propriétaire d’une maison spacieuse sur l’Ile de Montréal ou dans les environs, c’est peut-être plus rare qu’on le croit.</strong></p>

                            <p>Alors, pourquoi ne pas envisager une solution astucieuse et des plus populaires comme transformer votre duplex ou votre triplex en cottage? Ne serait-ce pas là l’aboutissement de notre réalité de plus en plus multigénérationnelle?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- TRANSFORMATION --- */ ?>
    <section id="transformation">
        <div class="container-fluid">
            <p class="title">Les travaux de rénovation de conversion ont la cote en ce momentet pour cause. On adapte nos appartements à la vie moderne dans un souci d’économie.</p>

            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up">
                        <div class="icon-container">
                            <img src="img/transformation/icon-patrimoine.png" alt="">
                        </div>

                        <h3>La préservation<br>du patrimoine</h3>

                        <p>Pour tirer le plein potentiel de votre plex et optimiser les volumes déjà existants, Robco Rénovation Construction conservera son «squelette» original. Votre habitation préservera son côté authentique, son cachet d’époque, sa touche de charme. Toutefois, sachez-le, Robco Rénovation Construction ne recule devant rien. Les maisons des années 1900, c’est son affaire.</p>
                    </div>  
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-container">
                            <img src="img/transformation/icon-decloisonner.png" alt="">
                        </div>

                        <h3>Décloisonner<br>l’espace</h3>

                        <p>Pour décloisonner vos espaces, créer des ouvertures et faire disparaître la sensation d’exiguïté, des modifications seront apportées à la structure du bâtiment. Les travaux de rénovation de conversion nécessitent de la démolition. L’abattement des murs porteurs implique de les remplacer par des poutres, des colonnes et des semelles pour compenser leur retrait. </p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon-container">
                            <img src="img/transformation/icon-air-ouverte.png" alt="">
                        </div>

                        <h3>Espace à<br>air ouverte</h3>

                        <p>Rien de mieux que la construction de pièces à aire ouverte qui communiquent au rez-de-chaussée telles l’entrée, la cuisine, la salle de séjour et le bureau par exemple, pour donner l’impression que l’espace est plus spacieux, voire encore plus vaste. Nous effectuons justement ce type d’intervention.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up">
                        <div class="icon-container">
                            <img src="img/transformation/icon-poutres.png" alt="">
                        </div>

                        <h3>Murs et poutres<br>porteuses</h3>

                        <p>Nous offrons le service de retrait de vos murs porteurs pour dégager l’espace afin qu’on se sente moins à l’étroit et que l’on puisse circuler plus librement. Les poutres porteuses se dissimulent et se fondent plus facilement dans le décor et permettent d’éviter le cloisonnement et l’obstruction de l’espace. Par le fait même, la lumière sera mieux distribuée et dispersée.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="200">
                        <div class="icon-container">
                            <img src="img/transformation/icon-escalier.png" alt="">
                        </div>

                        <h3>escalier reliant<br>les 2 étages</h3>

                        <p>Nous offrons également l’excavation du sous-sol pour creuser en profondeur. Et qui dit sous-sol dit escalier à installer. Nos charpentiers menuisiers sauront concevoir un bel escalier sur mesure pour relier vos deux étages. Et pour les étages supérieurs, nous pourrons au besoin le doter de superbes puits de lumière qui surplomberont votre cage d’escalier.</p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="single-trans" data-aos="fade-up" data-aos-delay="400">
                        <div class="icon-container">
                            <img src="img/transformation/icon-salles-de-bain.png" alt="">
                        </div>

                        <h3>création de<br>salles de bain</h3>

                        <p>Que ce soit pour déplacer ou créer une nouvelle salle de bain, nous possédons l’expertise et le matériel nécessaires pour procéder au déplacement de votre tuyauterie.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- TRANSFORMATION END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">La finition du<br>sous-sol, <br>un gros plus</h2>

                            <p>Investir le sous-sol pour y entreposer vos affaires, aménager un bar, un cellier, un cinéma maison, un atelier, une chambre à coucher avec salle de bain attenante, une salle familiale, de jeu, d’exercice, voilà toutes d’excellentes suggestions pour bonifier le sort des occupants.</p>

                            <p>Robco Rénovation Construction sait parfaitement que les agrandissements les plus réussis se fondent dans l’existant et rehaussent l’attrait du bâtiment extérieur. Pour un agrandissement sur fondations, des travaux d’excavation ou la surélévation de votre toit, Robco Rénovation Construction est ce qu’il vous faut.</p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/transformation/transformation-soussol.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>

</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'inc/realisations.php';
include 'footer.php';
include 'scripts.php';
?>