<?php
/* --- VARIABLES --- */
$meta_description = "";
$meta_keywords = "";
$page_title = "Agrandisssement de maison";
$body_class = "agr-content";
$page_fr = "agrandissement";
$page_en = "en/extensions";

/* --- INCLUDE HEADER --- */
include 'head.php';
include 'header.php';
?>

<?php /* --- MAIN START --- */ ?>
<main>
    
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1>agrandissement<br>de maisons</h1>

                            <p><strong>L’état actuel de votre maison ne vous convient plus, vous manquez d’espace, mais vous ne souhaitez pas déménager pour autant puisque vous aimez son emplacement? </strong></p>

                            <p>Vous souhaiteriez plutôt tirer parti de la résidence que vous possédez déjà? Vous préféreriez investir dans celle-ci et augmenter sa valeur patrimoniale?</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <?php /* --- RENOVATIONS --- */ ?>
    <section id="renovations" class="">
        <div class="container-fluid">
            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Les possibilités</h2>

                            <ul>
                                <li data-aos="fade-up"><strong>L’ajout d’un étage</strong><br>Si la portance du toit est suffisamment solide pour supporter un deuxième étage, ajoutez-en un au-dessus du garage.</li>
                                <li data-aos="fade-up"><strong>L’agrandissement en porte-à-faux</strong><br>Agrandissez une pièce de quatre pieds, un peu comme si on ajoutait une terrasse, et ce, sans le recours à l’excavation.</li>
                                <li data-aos="fade-up"><strong>L’agrandissement par-derrière</strong><br>Empiétez sur la cour arrière en réalisant un agrandissement qui s’étend tout le long de la largeur de la maison.</li>
                                <li data-aos="fade-up"><strong>Creuser le sous-sol</strong><br>Bénéficiez d’un espace supplémentaire et creusez votre sous-sol. Par la même occasion, profitez-en pour consolider vos fondations.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/agrandissement/agrandissement-possibilités.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno reverse">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="img-container" data-aos="fade-right" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/agrandissement/agrandissement-avantages.jpg" alt="">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="text-container small">
                            <h2 class="bordered">Les avantages</h2>

                            <p>Agrandir sa maison signifie aussi de ne pas devoir acheter une maison plus grande qui vous coûterait plus cher en chauffage et en électricité.</p>

                            <p>D’autant plus que, lors des travaux, on profite de l’occasion pour isoler le bâtiment et remplacer les anciens matériaux par de nouveaux plus écologiques ou moins dommageables pour la santé.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-reno">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-2">
                        <div class="text-container">
                            <h2 class="bordered">Attention!!!</h2>

                            <p>La plupart du temps, les agrandissements par l’avant sont interdits. Une extension qui n’est pas en harmonie avec le restant de la maison défigure le paysage et peut la dévaluer. Raison pour laquelle vous aurez besoin des conseils avisés d’un entrepreneur qualifié tel celui de Robco Rénovation Construction.</p>

                            <p>Si votre terrain et les règles d’urbanisme le permettent, vous pourriez agrandir par le haut, l’avant, l’arrière, les côtés ou tout simplement en aménageant un espace non utilisé. Il suffit d’ajouter un étage, d’exploiter les combles, votre garage ou encore le sous-sol. Et pourquoi ne pas en profiter pour construire une superbe mezzanine? Du même coup, votre habitation disposerait d’une meilleure lumière naturelle. </p>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12 col-sm-o-1">
                        <div class="img-container" data-aos="fade-left" data-aos-anchor-placement="center-bottom">
                            <div class="box"></div>
                            <img src="img/agrandissement/agrandissement-attention.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- RENOVATIONS END --- */ ?>
    
</main>
<?php /* --- MAIN END --- */ ?>

<?php
/* --- INCLUDE FOOTER --- */
include 'inc/realisations.php';
include 'footer.php';
include 'scripts.php';
?>