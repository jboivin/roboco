<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
add_theme_support('title-tag');
add_theme_support('post-thumbnails');

function enqueue_scripts() {
    wp_enqueue_style('style_2', '/../css/styles.css');
    wp_enqueue_script('bootstrap', '/../js/bootstrap.min.js', array('jquery'), null, true);
    wp_enqueue_script('aos', '/../js/aos.js', array('jquery'), null, true);
    wp_enqueue_script('paroller', '/../js/paroller.js', array('jquery'), null, true);
    wp_enqueue_script('scripts', '/../js/scripts.js', array('jquery'), null, true);
}

add_action('wp_enqueue_scripts', 'enqueue_scripts');

add_filter('body_class', 'multisite_body_classes');

function multisite_body_classes($classes) {
    $classes[] = 'move-out';
    return $classes;
}

// Register Custom Post Type
function projets_en_cours() {

    $labels = array(
        'name' => _x('Projets', 'Post Type General Name', 'robco'),
        'singular_name' => _x('Projet', 'Post Type Singular Name', 'robco'),
        'menu_name' => __('Projets', 'robco'),
        'name_admin_bar' => __('Projets', 'robco'),
        'archives' => __('Item Archives', 'robco'),
        'attributes' => __('Item Attributes', 'robco'),
        'parent_item_colon' => __('Parent Item:', 'robco'),
        'all_items' => __('All Items', 'robco'),
        'add_new_item' => __('Add New Item', 'robco'),
        'add_new' => __('Add New', 'robco'),
        'new_item' => __('New Item', 'robco'),
        'edit_item' => __('Edit Item', 'robco'),
        'update_item' => __('Update Item', 'robco'),
        'view_item' => __('View Item', 'robco'),
        'view_items' => __('View Items', 'robco'),
        'search_items' => __('Search Item', 'robco'),
        'not_found' => __('Not found', 'robco'),
        'not_found_in_trash' => __('Not found in Trash', 'robco'),
        'featured_image' => __('Featured Image', 'robco'),
        'set_featured_image' => __('Set featured image', 'robco'),
        'remove_featured_image' => __('Remove featured image', 'robco'),
        'use_featured_image' => __('Use as featured image', 'robco'),
        'insert_into_item' => __('Insert into item', 'robco'),
        'uploaded_to_this_item' => __('Uploaded to this item', 'robco'),
        'items_list' => __('Items list', 'robco'),
        'items_list_navigation' => __('Items list navigation', 'robco'),
        'filter_items_list' => __('Filter items list', 'robco'),
    );
    $args = array(
        'label' => __('Projet', 'robco'),
        'description' => __('Post Type Description', 'robco'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array('category', 'post_tag'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'rewrite' => array('slug' => false, 'with_front' => false)
    );
    register_post_type('projet', $args);
}

add_action('init', 'projets_en_cours', 0);

function pagination_bar() {
    global $test;
 
    $total_pages = $test->max_num_pages;
 
    if ($total_pages > 1){
        $current_page = max(1, get_query_var('paged'));
 
        echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
        ));
    }
}
