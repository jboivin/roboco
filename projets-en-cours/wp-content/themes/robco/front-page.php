<?php

function checkCurrentURL($pageName) {
    $currentURL = $_SERVER['PHP_SELF'];
    if (strpos($currentURL, $pageName) !== false) {
        return 'active';
    }
}

$prefix = "../";

get_header();
include '../header.php';
?>

<main>
    <?php /* --- SUBPAGE INTRO --- */ ?>
    <section id="subpage-intro" class="">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="img-container" style="background: url('<?php the_post_thumbnail_url(); ?>') center center/cover no-repeat;"></div>
                </div>

                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="text-container">
                        <div class="text-block">
                            <h1><?php echo get_field("titre"); ?></h1>

                            <?php if (get_field("sous-titre")): ?>
                                <p><strong><?php echo get_field("sous-titre"); ?></strong></p>
                            <?php endif; ?>

                            <p><?php echo get_field("description"); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php /* --- SUBPAGE INTRO END --- */ ?>

    <section id="projects">
        <div class="container-fluid">
            <div class="row">

                <?php
                $args = array(
                    'post_type' => 'projet'
                );
                $test = new WP_Query($args);
                while ($test->have_posts()) : $test->the_post();
                    ?>

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="single-proj">
                            <a class="img-container" href="<?php echo get_the_permalink(); ?>" title="" style="background: url('<?php the_post_thumbnail_url(); ?>') center center/cover no-repeat;"></a>

                            <h3><a href="<?php echo get_the_permalink(); ?>" title=""><?php echo the_title(); ?></a></h3>

                            <p><?php
                                if (get_field("apercu")) {
                                    echo get_field("apercu");
                                }
                                ?></p>

                            <a class="cust-btn" href="<?php echo get_the_permalink(); ?>" title="">voir le projet</a>
                        </div>
                    </div>

                    <?php
                endwhile;
                ?>
            </div>

            <nav class="pagination">
                <?php pagination_bar(); ?>
            </nav>
        </div>
    </section>
</main>

<?php
include '../footer.php';
get_footer();
?>