<?php

function checkCurrentURL($pageName) {
    $currentURL = $_SERVER['PHP_SELF'];
    if (strpos($currentURL, $pageName) !== false) {
        return 'active';
    }
}

$prefix = "../../../";

get_header();
include '../header.php';
?>

<?php
while (have_posts()) :
    the_post();
    ?>

    <main>
        <?php /* --- SUBPAGE INTRO --- */ ?>
        <section id="subpage-intro" class="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="img-container" style="background: url('<?php the_post_thumbnail_url(); ?>') center center/cover no-repeat;"></div>
                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="text-container">
                            <div class="text-block">
                                <h1><?php echo the_title(); ?></h1>

                                <p><?php echo get_field("description"); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php /* --- SUBPAGE INTRO END --- */ ?>

        <section id="single-proj">
            <div class="container-fluid">
                <?php
                if (have_rows('etapes_du_projet')):
                    while (have_rows('etapes_du_projet')) : the_row();
                        ?>

                        <div class="single-step">
                            <h2><?php echo get_sub_field("titre"); ?></h2>

                            <?php
                            if (have_rows('bloc')):
                                while (have_rows('bloc')) : the_row();
                                    ?>

                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="img-container" style="background: url('<?php echo get_sub_field("photo"); ?>') center center/cover no-repeat;"></div>
                                        </div>

                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="text-container">
                                                <?php echo get_sub_field("description"); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                endwhile;
                            endif;
                            ?>
                        </div>
                        <?php
                    endwhile;
                endif;
                ?>


                <div class="btn-container">
                    <a class="btn gray-btn" href="<?php echo get_home_url(); ?>" title="">Retour aux projets</a>
                </div>
            </div>
        </section>
    </main>

    <?php
endwhile;
?>

<?php
include '../footer.php';
get_footer();
?>