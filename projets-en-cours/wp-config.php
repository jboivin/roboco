<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'robco');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-G#7<>2k%=j2R#5RGJKhGaW;]l+!<nW`Ns$bR3S$=pi yA|_~qgUZ_Wz*fvMJ*t(');
define('SECURE_AUTH_KEY',  'h7zW[&NQ0dCQcT,utUXyN/J4^M7LHz?_!G(g.aMfUFd9*9W3`7d]Dv+&An~y,Wkr');
define('LOGGED_IN_KEY',    ';9sUGP?-tw)K>?7I !p;B!k,Z4*k|9ilc|R~>P]/Q?bO6^%Lv?*Gv(cppTsy7f7d');
define('NONCE_KEY',        '[ 2|waI.:rq,%1uWg&Q/3KWmwk1-/?eHBAY?9Wr{Z*F,4^p)lmn<7EQtB4ph{_C!');
define('AUTH_SALT',        'B)ZREno^yzh;l.Vg<uYds?%sRZe.<U)KEfay{U)n&+]|fbrbir9*jdD]ayC^V/e,');
define('SECURE_AUTH_SALT', '!-}/8^w,&:6}c6DpGQ7{`NV({e~hZ-4;79w:S)~9QWma.c*2u]~p)&1nKB#t/F4_');
define('LOGGED_IN_SALT',   '9~[YA+;:%8Ahi[3qw[/Kl>v0#,0Z6z@ {O>X9X/{ZX3fx@jL/_g~R8kw^B8[P@K%');
define('NONCE_SALT',       'Oo*3X0zxP12l /F%1rPUm)56Ryj9Jlf~X-e>(e#8o_E+.dZxf@5p :pbP:.ptKv/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'rb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
